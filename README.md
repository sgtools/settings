# Welcome to @sgtools/settings
[![Version](https://npm.bmel.fr/-/badge/sgtools/settings.svg)](https://npm.bmel.fr/-/web/detail/@sgtools/settings)
[![Documentation](https://img.shields.io/badge/documentation-yes-brightgreen.svg)](https://gitlab.com/sgtools/settings/-/blob/master/docs/README.md)
[![License: ISC](https://img.shields.io/badge/License-ISC-yellow.svg)](https://spdx.org/licenses/ISC)

> Settings handling for nodejs applications


## Install

```sh
npm install @sgtools/settings
```

## Usage

```sh
import { settings } from '@sgtools/settings';
```

Code documentation can be found [here](https://gitlab.com/sgtools/settings/-/blob/master/docs/README.md).


## Author

**Sébastien GUERRI** <sebastien.guerri@apps.bmel.fr>


## Issues

Contributions, issues and feature requests are welcome!

Feel free to check [issues page](https://gitlab.com/sgtools/settings/issues). You can also contact the author.


## License

This project is [ISC](https://spdx.org/licenses/ISC) licensed.
