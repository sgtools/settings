[@sgtools/settings](../README.md) / [Exports](../modules.md) / SettingItem

# Interface: SettingItem

## Hierarchy

* *DataItem*

  ↳ **SettingItem**

## Table of contents

### Properties

- [items](settingitem.md#items)

## Properties

### items

• **items**: [*SettingsItem*](settingsitem.md)<*any*\>[]

Defined in: index.ts:109
