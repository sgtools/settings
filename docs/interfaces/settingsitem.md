[@sgtools/settings](../README.md) / [Exports](../modules.md) / SettingsItem

# Interface: SettingsItem<T\>

## Type parameters

Name |
------ |
`T` |

## Hierarchy

* **SettingsItem**

## Table of contents

### Properties

- [cb](settingsitem.md#cb)
- [content](settingsitem.md#content)
- [name](settingsitem.md#name)
- [title](settingsitem.md#title)

## Properties

### cb

• **cb**: () => *Promise*<*any*\>

Defined in: index.ts:9

___

### content

• **content**: T

Defined in: index.ts:10

___

### name

• **name**: *string*

Defined in: index.ts:7

___

### title

• **title**: *string*

Defined in: index.ts:8
