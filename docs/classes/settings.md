[@sgtools/settings](../README.md) / [Exports](../modules.md) / Settings

# Class: Settings

## Hierarchy

* **Settings**

## Table of contents

### Constructors

- [constructor](settings.md#constructor)

### Properties

- [items](settings.md#items)

### Methods

- [define](settings.md#define)
- [defineBoolean](settings.md#defineboolean)
- [defineString](settings.md#definestring)
- [get](settings.md#get)
- [load](settings.md#load)
- [set](settings.md#set)
- [Get](settings.md#get)
- [Set](settings.md#set)

## Constructors

### constructor

\+ **new Settings**(): [*Settings*](settings.md)

**Returns:** [*Settings*](settings.md)

Defined in: index.ts:15

## Properties

### items

• **items**: [*SettingsItem*](../interfaces/settingsitem.md)<*any*\>[]

Defined in: index.ts:15

## Methods

### define

▸ **define**(`name`: *string*, `title`: *string*, `cb`: () => *Promise*<*any*\>): *void*

#### Parameters:

Name | Type |
------ | ------ |
`name` | *string* |
`title` | *string* |
`cb` | () => *Promise*<*any*\> |

**Returns:** *void*

Defined in: index.ts:33

___

### defineBoolean

▸ **defineBoolean**(`name`: *string*, `title`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`name` | *string* |
`title` | *string* |

**Returns:** *void*

Defined in: index.ts:43

___

### defineString

▸ **defineString**(`name`: *string*, `title`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`name` | *string* |
`title` | *string* |

**Returns:** *void*

Defined in: index.ts:56

___

### get

▸ **get**<T\>(`name`: *string*): T

#### Type parameters:

Name |
------ |
`T` |

#### Parameters:

Name | Type |
------ | ------ |
`name` | *string* |

**Returns:** T

Defined in: index.ts:93

___

### load

▸ **load**(): *void*

**Returns:** *void*

Defined in: index.ts:22

___

### set

▸ **set**<T\>(`name`: *string*, `value`: T): [*SettingsItem*](../interfaces/settingsitem.md)<T\>

#### Type parameters:

Name |
------ |
`T` |

#### Parameters:

Name | Type |
------ | ------ |
`name` | *string* |
`value` | T |

**Returns:** [*SettingsItem*](../interfaces/settingsitem.md)<T\>

Defined in: index.ts:82

___

### Get

▸ `Static`**Get**<T\>(`name`: *string*, `defaultValue`: T): *function*

#### Type parameters:

Name |
------ |
`T` |

#### Parameters:

Name | Type |
------ | ------ |
`name` | *string* |
`defaultValue` | T |

**Returns:** *function*

Defined in: index.ts:74

___

### Set

▸ `Static`**Set**<T\>(`name`: *string*): *function*

#### Type parameters:

Name |
------ |
`T` |

#### Parameters:

Name | Type |
------ | ------ |
`name` | *string* |

**Returns:** *function*

Defined in: index.ts:69
