[@sgtools/settings](README.md) / Exports

# @sgtools/settings

## Table of contents

### Classes

- [Settings](classes/settings.md)

### Interfaces

- [SettingItem](interfaces/settingitem.md)
- [SettingsItem](interfaces/settingsitem.md)

### Variables

- [settings](modules.md#settings)

## Variables

### settings

• `Const` **settings**: [*Settings*](classes/settings.md)

Defined in: index.ts:133
