"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.settings = exports.Settings = void 0;
const data_1 = require("@sgtools/data");
const data_2 = require("@sgtools/data");
const console_1 = require("@sgtools/console");
class Settings {
    constructor() {
        this.items = [];
    }
    load() {
        if (data_1.data.settings && data_1.data.settings.length === 1) {
            let savedItems = data_1.data.settings[0].items;
            savedItems.forEach(savedItem => {
                let item = this.items.find(i => i.name === savedItem.name);
                item.content = savedItem.content;
            });
        }
    }
    define(name, title, cb) {
        this.items.push({
            name: name,
            title: title,
            cb: cb,
            content: null
        });
    }
    defineBoolean(name, title) {
        this.define(name, title, async () => {
            console_1.console.newline();
            console_1.console.info(title);
            console_1.console.print(`[i][y]Current value :[/y][/i] ${exports.settings.get(name) ? 'true' : 'false'}`);
            await console_1.console.question('New value    ').then((res) => {
                exports.settings.set(name, res.trim() === 'true');
            });
            return true;
        });
    }
    defineString(name, title) {
        this.define(name, title, async () => {
            console_1.console.newline();
            console_1.console.info(title);
            console_1.console.print(`[i][y]Current value :[/y][/i] ${exports.settings.get(name)}`);
            await console_1.console.question('New value    ').then((res) => {
                exports.settings.set(name, res);
            });
            return true;
        });
    }
    static Set(name) {
        return (value) => { exports.settings.set(name, value); };
    }
    static Get(name, defaultValue) {
        return () => {
            let content = exports.settings.get(name);
            return (typeof content === 'undefined') ? defaultValue : content;
        };
    }
    set(name, value) {
        let item = this.items.find(i => i.name === name);
        if (!item)
            return null;
        item.content = value;
        data_1.data.updateSettings();
        return item;
    }
    get(name) {
        let item = this.items.find(i => i.name === name);
        if (!item)
            return null;
        return item.content;
    }
}
exports.Settings = Settings;
let pluginName = 'settings';
data_2.Data.prototype.settings = [];
data_2.Data.prototype.updateSettings = () => {
    data_2.Data.SetItem(pluginName, { items: exports.settings.items });
};
data_2.Data.addPlugin(pluginName);
exports.settings = new Settings();
