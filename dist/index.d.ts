import { DataItem } from '@sgtools/data';
export interface SettingsItem<T> {
    name: string;
    title: string;
    cb: () => Promise<any>;
    content: T;
}
export declare class Settings {
    items: SettingsItem<any>[];
    constructor();
    load(): void;
    define(name: string, title: string, cb: () => Promise<any>): void;
    defineBoolean(name: string, title: string): void;
    defineString(name: string, title: string): void;
    static Set<T>(name: string): (value: T) => void;
    static Get<T>(name: string, defaultValue: T): () => T;
    set<T>(name: string, value: T): SettingsItem<T>;
    get<T>(name: string): T;
}
export interface SettingItem extends DataItem {
    items: SettingsItem<any>[];
}
declare module '@sgtools/data' {
    interface Data {
        settings: SettingItem[];
        updateSettings(): void;
    }
}
export declare const settings: Settings;
