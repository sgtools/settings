import { data } from '@sgtools/data';
import { Data, DataItem } from '@sgtools/data';
import { console } from '@sgtools/console';

export interface SettingsItem<T>
{
    name    : string;
    title   : string;
    cb      : () => Promise<any>;
    content : T;
}

export class Settings
{
    items: SettingsItem<any>[];

    constructor()
    {
        this.items = [];
    }

    public load()
    {
        if (data.settings && data.settings.length === 1) {
            let savedItems = data.settings[0].items;
            savedItems.forEach(savedItem => {
                let item = this.items.find(i => i.name === savedItem.name);
                item.content = savedItem.content;
            });
        }
    }

    public define(name: string, title: string, cb: () => Promise<any>)
    {
        this.items.push({
            name    : name,
            title   : title,
            cb      : cb,
            content : null
        });
    }

    public defineBoolean(name: string, title: string)
    {
        this.define(name, title, async () => {
            console.newline();
            console.info(title);
            console.print(`[i][y]Current value :[/y][/i] ${settings.get<boolean>(name) ? 'true' : 'false'}`);
            await console.question('New value    ').then((res: string) => {
                settings.set<boolean>(name, res.trim() === 'true');
            });
            return true;
        });    
    }

    public defineString(name: string, title: string)
    {
        this.define(name, title, async () => {
            console.newline();
            console.info(title);
            console.print(`[i][y]Current value :[/y][/i] ${settings.get<string>(name)}`);
            await console.question('New value    ').then((res: string) => {
                settings.set<string>(name, res);
            });
            return true;
        });    
    }

    public static Set<T>(name: string)
    {
        return (value: T) => { settings.set(name, value); }
    }

    public static Get<T>(name: string, defaultValue: T)
    {
        return () => {
            let content = settings.get<T>(name);
            return (typeof content === 'undefined') ? defaultValue : content;
        };
    }

    public set<T>(name: string, value: T): SettingsItem<T>
    {
        let item = this.items.find(i => i.name === name);
        if (!item) return null;

        item.content = value;
        data.updateSettings();

        return item as SettingsItem<T>;
    }

    public get<T>(name: string): T
    {
        let item = this.items.find(i => i.name === name);
        if (!item) return null;

        return item.content as T;
    }

}



let pluginName = 'settings';

export interface SettingItem extends DataItem
{
    items: SettingsItem<any>[];
}

declare module '@sgtools/data'
{
    export interface Data
    {
        settings: SettingItem[];
        updateSettings(): void;
    }
}

Data.prototype.settings = [];

Data.prototype.updateSettings = () =>
{
    Data.SetItem<SettingItem>(pluginName, { items: settings.items });
}

Data.addPlugin(pluginName);




export const settings = new Settings();